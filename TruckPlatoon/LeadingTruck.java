import java.awt.Point;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *@name LeadingTruck: This class inherits super class "Truck" .
 *It fetch the vehicle parameters as speed , acceleration, coordinates, gap between trucks at run time
 * 
 * @version 02/25/2021
 */

public class LeadingTruck extends Truck
{
	private double X;                                                /*store the x coordinate of the truck*/
	private double Y;                                                /*store the Y coordinate of the truck*/
	private double speed;                                            /*store the speed of the truck*/
	private double acceleration;                                     /*store the acceleration of the truck*/
	private boolean brake;                                           /*store the brake status of the truck*/
	private double turnAngle;                                        /*store the turning angle of the steering*/
	private ArrayList<FollowingTruck> foltrucks;                     /*store the list of following truck*/
	public final double MIN_GAP_IN_BETWEEN_TRUCKS_IN_PLATOON = 6.0;  /*minimum Gap need to be maintained between the trucks*/
    private ArrayList<Double> SensorData = new ArrayList<Double>(); /*creates list of sensor data*/
	private ArrayList<Point2> GPSData = new ArrayList<Point2>();    /*creates list of GPS data*/

    /**
    * @name: Constructor for LeadTruck
    */

	public LeadingTruck() throws IOException
	{
		super();
		this.foltrucks = new ArrayList<FollowingTruck>();     /*creates list of following truck*/
		GPSData = super.readGPSData();                         /*Fetch the run time GPS data and store in ArrayList*/
	}

	/**
	* @name: Function to send the following truck parameters 
	* @param: Return the parameters of the following truck. 
	*/	
	public ArrayList<FollowingTruck> getArrayOfFTs()
	{
		return foltrucks;
	}
	
	/**
	* @name: Function to add the following truck  based on position in List
	* @param: Index at which the following truck need to be added in list
	* @param: Truck Information which need to be added in list
	*/	
	public void addFolTruck(int position, FollowingTruck truck)
	{
		getArrayOfFTs().add(position, truck);
	}

	/**
	* @name: Function to update the following truck parameters based on position in List
	* @param: Index at which the following truck parameter need to be updated in list
	* @param: Following truck parameters which need to updated in existing list
	*/	
	public void setFolTruck(int position, FollowingTruck truck)
	{
		getArrayOfFTs().set(position,truck);		
	}
	
	/**
	* @name: Function to fetch the position of following truck
	* @param: Position of the following truck
	* @param: Reurns the position of the following truck
	*/
	public FollowingTruck getFolTruck(int position)
	{
		return getArrayOfFTs().get(position);
	}
	
	/**
	* @name: Function to calculate the gap between the truck
	* @param: Get the truck number whose coordinate need to be fetched
	* @param: Reurns the gap between the truck
	*/
	public double calculateGap(int truckNumber)
	{
		double x = (getFolTruck(truckNumber)).getXcoord();
		double y = (getFolTruck(truckNumber)).getYcoord();
		double distance = Math.sqrt((y-getYcoord())*(y-getYcoord()) + 
				(x-getXcoord())*(x-getXcoord())) - (truckNumber+1)*LENGTH_OF_TRUCK;
		return distance;
	}
	
	/**
	* @name: Function to fetch the runtime data and store the vehicle parameter in a list
	*/		
	public String runTimeData()
	{


		ArrayList<String> currentValues = new ArrayList<String>();
		currentValues.add(0, "X: "+ getXcoord());                /*X, Y of Lead Truck will always be 0,0 wrt to other following trucks*/
		currentValues.add(1, "Y: "+ getYcoord());
		currentValues.add(2, "speed: "+ getSpeed());
		currentValues.add(3, "acceleration: "+ getAcceleration());
		currentValues.add(4, "brake: "+ getBrakeStatus());
		currentValues.add(5, "turnAngle: "+ getTurnAngle());
		return currentValues.toString();
	}	 	
  
}
