import mpi.*;           // for mpiJava

import java.io.IOException;
import java.net.*;      // for InetAddress
import java.util.*;     // for Date
import java.io.BufferedWriter;
import java.io.File;  // Import the File class
import java.io.FileWriter;
import java.awt.Point;

/* Class to handle all the MPI Computations required in Truck Platooning Project*/
public class MPIComputation {
    // mpi-related values
    int myrank = 0;;
    int nprocs = 0;

	//public static LeadingTruck TruckLead;

	double counter[] = null;
	double counter2[] = null;
	int index = 0;
    // matrices
    double a[] = null; // array must be one dimensional in mpiJava.
    double b[] = null; // array must be one dimensional in mpiJava.
    double c[] = null; // array must be one dimensional in mpiJava.

    // message component
    int averows;               // average #rows allocated to each rank
    int extra;                 // extra #rows allocated to some ranks
    int offset[] = new int[1]; // offset in row
    int rows[] = new int[1];   // the actual # rows allocated to each rank
    int start[] = new int[1];
    int end[] = new int[1];
    public int mtype;                 // message type (tagFromMaster or tagFromWorker )

    final static int tagFromMaster = 1;
    final static int tagFromWorker = 2;
    final static int master = 0;

    // print option
    boolean printOption = true; // print out all array contents if true

    /**
     * Initializes matrices.
     * @param size the size of row/column for each matrix
     */
    private void init_array(int size)
    {
    	//counter = new int[size];
    	for ( int i = 0; i < size; i++ )
    	{
    		counter[i] = 0.0;
    		counter2[i] = 0.0;
    	}
    	
    }
     /**
     * Initializes matrices.
     * @param size the size of row/column for each matrix
     */

    private void init( int size ) {
	// Initialize matrices

	for ( int i = 0; i < size; i++ )
	    for ( int j = 0; j < size; j++ )
		a[i * size + j] = i + j;       // a[i][j] = i + j;
	for ( int i = 0; i < size; i++ )
	    for ( int j = 0; j < size; j++ )
		b[i * size + j] = i - j;       // b[i][j] = i - j;
	for ( int i = 0; i < size; i++ )
	    for ( int j = 0; j < size; j++ )
		c[i * size + j] = 0;           // c[i][j] = 0
    }
    /**
     * Increment the counters.
     * @param size the size of row/column for each matrix
     */
    private void increment(int size)
    {
    	for ( int k = 0; k < end[0]; k++ )
    	{
    			counter[k] += k*1.23;
        		counter2[k] += k*1.24;
    	}

    }
    /**
     * Computes a multiplication for my allocated rows.
     * @param size the size of row/column for each matrix
     */
    private void compute( int size ) {
	
	for ( int k = 0; k < size; k++ )
	    for ( int i = 0; i < rows[0]; i++ )
		for ( int j = 0; j < size; j++ ) {
		    // c[i][k] += a[i][j] * b[j][k]
		    c[i * size + k] += a[i * size + j] * b[j *size + k];
		}
    }

    /**
     * Prints out all elements of a given array.
     * @param array an array of doubles to print out
     */
    private void print( double array[] ) {
	if ( myrank == 0 && printOption == true ) {
	    int size = ( int )Math.sqrt( ( double )array.length );
	    for ( int i = 0; i < size; i++ )
	    {
		for ( int j = 0; j < size; j++ ) {
		    // Sytem.out.println( array[i][j] );
		    System.out.print( "[" + i + "]"+ "[" + j + "] = " 
					+ array[i * size + j] );
		}
	    System.out.println();
	    }
	}
    }

	/**
     * Is the constructor to initialize the array sizes
     * @param size the size of row/column for each matrix
     */
    
    public MPIComputation(int size)
    {
    	counter = new double[size*size];
    	counter2 = new double[size*size];

    }
	/**
     * Is the constructor that implements master-worker matrix transfers and
     * matrix multiplication.
     * @param option the option to print out all matrices ( print if true )	 
     * @param size the size of row/column for each matrix
     */
    public MPIComputation(int size, boolean option) throws MPIException {
	myrank = MPI.COMM_WORLD.Rank();
	nprocs = MPI.COMM_WORLD.Size();

	a = new double[size*size];  // a = new double[size][size]
	b = new double[size*size];  // b = new double[size][size]
	c = new double[size*size];  // c = new double[size][size]

	printOption = option;

	if (myrank == 0) {
	    // I'm a master.

	    // Initialize matrices.
	    init( size );
	    System.out.println("array a:");
	    print(a);

	    System.out.println( "array b:" );
	    print(b);

	    // Construct message components.
	    averows = size / nprocs;
	    extra = size % nprocs;
	    offset[0] = 0;
	    mtype = tagFromMaster;

	    // Start timer.
	    Date startTime = new Date();

	    // Trasfer matrices to each worker.
	    for ( int rank = 0; rank < nprocs; rank++ ) {
		rows[0] = ( rank < extra ) ? averows + 1 : averows;
		System.out.println( "sending " + rows[0] + " rows to rank " + rank );
		if ( rank != 0 ) {
		    MPI.COMM_WORLD.Send( offset, 0, 1, MPI.INT, rank, mtype );
		    MPI.COMM_WORLD.Send( rows, 0, 1, MPI.INT, rank, mtype );
		    MPI.COMM_WORLD.Send( a, offset[0] * size, rows[0] * size,
					 MPI.DOUBLE, rank, mtype );
		    MPI.COMM_WORLD.Send( b, 0, size * size, MPI.DOUBLE, rank, 
					 mtype );
		}
		offset[0] += rows[0];
	    }

	    // Perform matrix multiplication.
	    compute( size );

	    // Collect results from each worker.
	    int mytpe = tagFromWorker;
	    for ( int source = 1; source < nprocs; source++ ) {
		MPI.COMM_WORLD.Recv( offset, 0, 1, MPI.INT, source, mtype );
		MPI.COMM_WORLD.Recv( rows, 0, 1, MPI.INT, source, mtype );
		MPI.COMM_WORLD.Recv( c, offset[0] * size, rows[0] * size, MPI.DOUBLE, source, mtype );
	    }

	    // Stop timer.
	    Date endTime = new Date();

	    // Print out results
	    System.out.println( "result c:" );
	    print(c);
	    try {
	    	BufferedWriter outputWriter = null;
	    	  outputWriter = new BufferedWriter(new FileWriter("ArrayResults.txt"));
	    	  for (int i = 0; i < c.length; i++) {
	    	    outputWriter.write(c[i]+"");
	    	    outputWriter.newLine();
	    	    
	    	  }
	    	  outputWriter.flush();  
	    	  outputWriter.close();
	        System.out.println("Successfully wrote to the file.");
	      } catch (IOException e) {
	        System.out.println("An error occurred.");
	        e.printStackTrace();
	      }
	    System.out.println( "time elapsed = " +
				( endTime.getTime() - startTime.getTime() ) +
				" msec" );
	}
	else {
	    // I'm a worker.
	    
	    // Receive matrices.
	    int mtype = tagFromMaster;
	    MPI.COMM_WORLD.Recv( offset, 0, 1, MPI.INT, master, mtype );
	    MPI.COMM_WORLD.Recv( rows, 0, 1, MPI.INT, master, mtype );
	    MPI.COMM_WORLD.Recv( a, 0, rows[0] * size, MPI.DOUBLE, master, 
				 mtype );
	    MPI.COMM_WORLD.Recv( b, 0, size * size, MPI.DOUBLE, master, 
				 mtype );
	    
	    // Perform matrix multiplication.
	    compute(size);

	    // Send results to the master.
	    MPI.COMM_WORLD.Send(offset, 0, 1, MPI.INT, master, mtype );
	    MPI.COMM_WORLD.Send(rows, 0, 1, MPI.INT, master, mtype );
	    MPI.COMM_WORLD.Send( c, 0, rows[0] * size, MPI.DOUBLE, master, mtype);
	}
    }
	/**
     * Is the function that implements master-worker array transfes and
     * simulates data as such as GPS data. 
     * @param size the size of row/column for each matrix
     */
    public void computeGPS(int size)
    {
    	myrank = MPI.COMM_WORLD.Rank();
    	nprocs = MPI.COMM_WORLD.Size();
    	        
        if (myrank == 0) {
        	mtype = tagFromMaster;
        	
        	init_array(size*size);
        	offset[0] = 0;
    	    start[0] = 0;
	    	end[0] = size - 1;           
    	    for ( int rank = 0; rank < nprocs; rank++ ) {

    			if ( rank != 0 ) {
        	    	start[0] = rank*size*10;
        	    	end[0] += size*10;
    				//MPI.COMM_WORLD.Send(offset, 0, 1, MPI.INT, rank, mtype);
    			    //MPI.COMM_WORLD.Send(counter, rank*size, size, MPI.INT, rank, mtype);
    			    if(end[0]>=size*size)end[0]=size*size-1;
    				MPI.COMM_WORLD.Send(counter, 0, size*size, MPI.DOUBLE, rank, mtype);
    				MPI.COMM_WORLD.Send(counter2, 0, size*size, MPI.DOUBLE, rank, mtype);
    				MPI.COMM_WORLD.Send(start, 0, 1, MPI.INT, rank, mtype);
    				MPI.COMM_WORLD.Send(end, 0, 1, MPI.INT, rank, mtype);
    				System.out.println( "sending start index " + start[0] +"end index " + end[0] + " to rank " + rank );
    			}
    			//offset[0]+=1;
    		}
    	    
    	    start[0] = 0;
	    	end[0] = size*10 - 1;
    	    increment(size*size);
    	    // Collect results from each worker.
    	    int mytpe = tagFromWorker;
    	    for ( int source = 1; source < nprocs; source++ ) {
    		//MPI.COMM_WORLD.Recv( counter, source*size, size, MPI.INT, source, mtype );
    		MPI.COMM_WORLD.Recv( counter, 0, size*size, MPI.DOUBLE, source, mtype );
    		MPI.COMM_WORLD.Recv( counter2, 0, size*size, MPI.DOUBLE, source, mtype );
    	    MPI.COMM_WORLD.Recv(start, 0, 1, MPI.INT, source, mtype );
    	    MPI.COMM_WORLD.Recv(end, 0, 1, MPI.INT, source, mtype );
    	    }
    	    
    	    try {
    	    	BufferedWriter outputWriter = null;
    	    	  outputWriter = new BufferedWriter(new FileWriter("GPSResults.txt"));
    	    	  for (int i = 0; i < size*size; i++) {
    	    	    outputWriter.write(counter[i]+" " + counter2[i]);
    	    	    outputWriter.newLine();
    	    	  }
    	    	  outputWriter.flush();  
    	    	  outputWriter.close();
    	        System.out.println("Successfully wrote to the file.");
    	      } catch (IOException e) {
    	        System.out.println("An error occurred.");
    	        e.printStackTrace();
    	      }
        }
        else {
    	    // I'm a worker.
    	    
    	    int mtype = tagFromMaster;
    	    //MPI.COMM_WORLD.Recv(counter, myrank*size, size, MPI.INT, master, mtype );
    	    MPI.COMM_WORLD.Recv(counter, 0, size*size, MPI.DOUBLE, master, mtype );
    	    MPI.COMM_WORLD.Recv(counter2, 0, size*size, MPI.DOUBLE, master, mtype );
    	    MPI.COMM_WORLD.Recv(start, 0, 1, MPI.INT, master, mtype );
    	    MPI.COMM_WORLD.Recv(end, 0, 1, MPI.INT, master, mtype );
    	    increment(size*size);
    	    // Send results to the master.   	    
			MPI.COMM_WORLD.Send(counter, 0, size*size, MPI.DOUBLE, master, mtype);
			MPI.COMM_WORLD.Send(counter2, 0, size*size, MPI.DOUBLE, master, mtype);
			MPI.COMM_WORLD.Send(start, 0, 1, MPI.INT, master, mtype);
			MPI.COMM_WORLD.Send(end, 0, 1, MPI.INT, master, mtype);
    	}
        
    	
    }
    /**
     *
     * @param args Receive the matrix size and the print option in args[0] and
     *             args[1]
     * @throws IOException 
     */
	public static void main(String[] args) throws MPIException
	{ 
		  
      MPI.Init( args );
	  int size[] = new int[1];
		boolean option[] = new boolean[1];
		option[0] =  false;

		if ( MPI.COMM_WORLD.Rank() == 0 ) {
		    // arg[] starts from args[1] in mpiJava.
		    size[0] = Integer.parseInt( args[1] ); 
		    if ( args.length == 5 ) // args.length increment by 2
			option[0] = true;
		}

		// Broadcast size and option to all workers.
		MPI.COMM_WORLD.Bcast( size, 0, 1, MPI.INT, master);
		MPI.COMM_WORLD.Bcast( option, 0, 1, MPI.BOOLEAN, master);
      
        	
		// Compute matrix multiplication in both master and workers.
		new MPIComputation(100,option[0]);
		
		
		new MPIComputation(100).computeGPS(100);
		
		// Terminate the MPI library.
		MPI.Finalize();
	}
    
}
