import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;
/**
 * @name LeadTruckConnection : This class handles to establish the communication between client and server.
 * and process the received message from the server and the client.This class implements runnable interface and
 * implements the run() method which is executed by the main thread of TcpServer in an
 * infinite loop for server trucks.
 * 
 * @version 02/25/2021
 */


/**
 * @name: Function to establish communication between server and client
 */
public class LeadTruckConnection implements Runnable 
{
  private Socket server;                                     /*socket to receive message from client/following truck*/
  private BufferedReader in;                                 /*Buffer of default size used to read the message received from client and put in buffer for further processing*/
  private PrintWriter out;                                   
  private final int MAX_NUM_OF_RETRIES_COMM_FAILURE = 50;     /*Total waiting period in case of communication failure*/
  private int commCounter;                                    /*Counter to keep track of the maximum tries allowed before going to communication failure state*/
  private FollowingTruck ftruck;
  private static int counter = 0;
  private ArrayList<Point2> GPSData = new ArrayList<Point2>();   /*To store the list of GPS data being generated from MPIComputation at runtime*/
  private final static ReentrantLock lock = new ReentrantLock();
	
  /**
   * @name: Constructor for LeadTruckConnection
   * @param leadTruck: socket parameters - IP and Port no
   */
  
  public LeadTruckConnection(Socket leadTruck) throws IOException
  {
	  this.server = leadTruck;
	  in = new BufferedReader(new InputStreamReader(server.getInputStream()));  /*To get the data from client*/
	  out = new PrintWriter(server.getOutputStream(),true);                     /*To send the data to client*/
	  this.commCounter = 0;	  
	  this.ftruck = new FollowingTruck();
	  GPSData = ftruck.readGPSData();                                           /*To store the GPS data*/
  }
  
  /**
   * @name: Function to extract the individual position and truck number from the message 
   * @param msg: Hold the message sent from the server
   */
  
  public int extractPos(String msg)
  { 
	  int pos = 0;
	  if(msg.contains("truck"))
	  {
		  pos = msg.indexOf("truck")+6;
		  String name = msg.substring(pos,pos+1);
		  pos = Integer.parseInt(name);
	  }
	  return pos;	  
  }
  
  /**
   * @name: Function to match the behavior of lead truck with following truck
   * @param position : hold the position of following truck
   * @param msg: Hold the message received from the server
   */  
  
  public void matchLeadTruckBehavior(int position, String msg)
  {
		ftruck.setPosition(position);
		ftruck.setGap(position);
		ftruck.setTurnAngle(extractValue(msg, "turnAngle"));
		ftruck.setSpeed(extractValue(msg, "speed"));
		ftruck.setAcceleration(extractValue(msg, "acceleration"));
		ftruck.setBrakeStatus(isBrakeApplied(msg));
		ftruck.setCommStatus(true);
		ftruck.move(ftruck.getGap(),1);	  
  }
  
  /**
   * @name: Thread function, invoked automatically once thread start executing, 
   * @param msg: Hold the message sent from the server
   */
	public void run() 
	{
		/*Stores the run time data of the following truck*/
		String msg = ftruck.runTimeData();
		
		/*message sent to client*/
		out.println("UPDATE "+ msg);
		
		int counter = 0;
		try 
		{
			String response = "";
			int position = 0;
			while(true)
			{
				counter++;
				response = in.readLine();		
			 
				if(response != null) 
				{
					System.out.println("[LEAD TRUCK] Sent: "+ response);
					commCounter = 0;
					position = extractPos(response);
					
					updatecoordinates(position);
					out.println("UPDATE from truck "+ position + msg);
					
	                if(response.startsWith("UPDATE"))
	                {
	                	matchLeadTruckBehavior(position, response);
	                }
	                if(response.contains("ALERT"))
	                {
	                	msg = "Speed Reduced, Applying brake gradually";
						out.println("ACK "+msg);
	                	ftruck.setBrakeStatus(true);
	                }
	                else if(response.contains("Right"))
	                {
	                	msg = "Taking Right 45 Deg";
						out.println("ACK "+msg);
	                }
	                else if(response.contains("Left"))
	                {
	                	msg = "Taking Left 45 Deg";
						out.println("ACK "+msg);
	                }
	                else if(response.contains("Red, Apply Brakes"))
	                {
	                	msg = "Traffic Light Detected, Applying Brakes";
						out.println("ACK "+msg);
	                }
	                else if(response.contains("Red, Wait"))
	                {
	                	msg = "Waiting for the signal from Traffic Light";
						out.println("ACK "+msg);
	                }
	                else if(response.contains("Green"))
	                {
	                	msg = "Getting out from the Traffic";
						out.println("ACK "+msg);
	                }
	                
	                else
	                {
	                	msg = ftruck.runTimeData();
	                	out.println("ACK "+msg);
	                	ftruck.setBrakeStatus(false);
	                }
					
					  msg = ftruck.runTimeData(); 
					  out.println("ACK from truck "+ position + msg);
					 
					try {
						Thread.sleep(250);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					
					
				}
				else
				{
					/*check the communication failure based on counter value , if counter reaches 60 then commuincation failure is confirmed*/
					if(commCounter < MAX_NUM_OF_RETRIES_COMM_FAILURE)
					{
						commCounter++;
					}
					else
					{
						ftruck.setCommStatus(false);
						System.out.println("Communication failure from Server");
						
					}
				}								
			}
		} 
		catch (IOException e ) {
			e.printStackTrace();
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
/**
* @name: Function to extract the message from the received message
* @param msg : Holds the messages to be extracted
* @param valueToExtract: Holds the parameter which need to be extracted from the received message
*/  
	public double extractValue(String msg, String valueToExtract)
	{
		double val = 0.0;
		if(msg.contains(valueToExtract))
		{
			int index = msg.indexOf(valueToExtract)+valueToExtract.length()+2;
			String value = msg.substring(index,index+3);
			val = Double.parseDouble(value);
		}
		return val;		
	}

/**
* @name: Function to get the status of brake whether is applied or not successfully
* @param msg :Message store the information whether brake need to be applied or not
* @param val: returns true if brake need to be applied else false
*/
	public boolean isBrakeApplied(String msg)
	{
		boolean val = false;
		if(msg.contains("brake"))
		{
			int index = msg.indexOf("brake")+7;
			String value = msg.substring(index,index+4);
			if(value.equals("true"))
				val = true;
		}
		return val;		
	}
/**
* @name: Function to get run time coordinate of trucks
* @param position :Holds the position of trucks
*/	
	public void updatecoordinates(int position)
	{
		    Controller.incCounter();
			if(Controller.getCounter()>=GPSData.size())
			{
				Controller.setCounter(0);
				GPSData = ftruck.readGPSData();
			}
			ftruck.setXCoord(GPSData.get(Controller.getCounter()).x + 31*(position+1));
			ftruck.setYCoord(GPSData.get(Controller.getCounter()).y + 31*(position+1));
	}
		
}
	

  
  

