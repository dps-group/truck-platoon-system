import java.awt.Point;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;
/*@name: Controller 
 *Class to maintain Global Counters for indexing in Truck Platoon
 */
public class Controller {
	private static int counter = 0;
	private static int gpsCounter = 0;
	private final static ReentrantLock lock = new ReentrantLock();

   /**
     * Returns the static variable counter
     * @return counter: static variable to maintain same value across the platoon	 
     */
	public synchronized static int getCounter()
	{
		return counter;
	}
   /**
     * inrements the static variable counter
     */
	public synchronized static void incCounter()
	{
		try {
			lock.lock();
			counter++;
		}
		finally {
			lock.unlock();
		}
	
	}
    /**
     * sets the static variable counter to a specific value
     * @param value: value to be set to the variable counter
     */	
	public  synchronized static void setCounter(int value)
	{
		try {
			lock.lock();
			counter = value;
		}
		finally {
			lock.unlock();
		}
		
	}
   /**
     * inrements the static variable GPSCounter
     */	
	public synchronized static void incGPSCounter()
	{
		gpsCounter++;
	}
   /**
     * Returns the static variable GPSCounter
     * @return gpsCounter: static variable to maintain same value across the platoon	 
     */	
	public synchronized static int getGPSCounter()
	{
		return gpsCounter;
	}
    /**
     * sets the static variable GPSCounter to a specific value
     * @param value: value to be set to the variable GPSCounter
     */		
	public synchronized static void setGPSCounter(int value)
	{
		gpsCounter = value;
	}

}
