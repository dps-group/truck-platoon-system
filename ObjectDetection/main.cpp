#include "lodepng.h"
#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <ctime>
#include <fstream>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <helper_functions.h>
using namespace std;
//#include "kernel.cu"
extern void filter(unsigned char* input_image, unsigned char* output_image, int width, int height);


int main() {

    srand(time(0));

    //int random_variable = rand() / ((RAND_MAX) / 3);
    int counter = 0;
    while (true)
    {
        int random_variable = (rand() % 20) + 1;
        const char* input_file;
        printf("%d\n", random_variable);
        switch (random_variable)
        {
        case 1:
            input_file = "roadwithpedestrian.png";
            break;
        case 2:
            input_file = "car.png";
            break;
        case 3:
            input_file = "road.png";
            break;
        case 4:
            input_file = "right.png";
            break;
        case 5:
            input_file = "left.png";
            break;
        case 6:
            input_file = "trafficlight.png";
            break;
        default:
            input_file = "road.png";
            break;
        }


        std::vector<unsigned char> in_image;
        unsigned int width, height;
        //input_file = "trafficlight.png";
        // Load the data
        unsigned error = lodepng::decode(in_image, width, height, input_file);
        if (error) std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;

        // Prepare the data
        unsigned char* input_image = new unsigned char[(in_image.size() * 3) / 4];
        unsigned char* output_image = new unsigned char[(in_image.size() * 3) / 4];
        int where = 0;
        for (int i = 0; i < in_image.size(); ++i) {
            if ((i + 1) % 4 != 0) {
                input_image[where] = in_image.at(i);
                output_image[where] = 255;
                where++;
            }
        }

        // Run the filter on it
        filter(input_image, output_image, width, height);

        int sum = 0;
        // Prepare data for output
        std::vector<unsigned char> out_image;
        for (int i = 0; i < in_image.size() * 2 / 3; ++i) {

            sum += (int)output_image[i];
            out_image.push_back(output_image[i]);
            //sum += (int)out_image.at(i);

            if ((i + 1) % 3 == 0) {
                out_image.push_back(255);
            }
        }
        double average = sum / in_image.size();
        std::ofstream myfile;

        myfile.open("ObstacleDetection.txt", std::ios_base::app);
       
        if (myfile.is_open())
        {
            if (average < 27 && average > 19)
            {
                myfile << "Clear Road Detected\n";
            }
            else if (average < 44 && average > 39)
            {
                myfile << "Road with Pedestrian Detected\n";
            }
            else if (average > 45 && average < 50)
            {
                myfile << "Vehicle in front of Truck Detected\n";
            }
            else if (average > 65 && average < 68)
            {
                myfile << "Take Right\n";
            }
            else if (average > 105 && average < 110)
            {
                myfile << "Take Left\n";
            }
            else if (average > 160 && average < 165)
            {
                myfile << "Traffic Light Detected\n";
            }

            myfile.close();
        }
        // Output the data
       //error = lodepng::encode(output_file, out_image, width, height);

       //if there's an error, display it
       //if (error) std::cout << "encoder error " << error << ": " << lodepng_error_text(error) << std::endl;

        counter++;
        printf("%d times done\n", counter);
        printf("Average: %f\n", average);
        delete[] input_image;
        delete[] output_image;
    }

    return 0;

}